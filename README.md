# EUNOMIA ChainCode

This is the key-value store chaincode for EUNOMIA's Blockchain Infrastructure.

## Implementation

The system saves the entries as json serialized string.

System implemends the following functions:

---

### QueryEntry

#### Summary
Returns the entry with key Type + ID

#### Arguments
* Type       : Type of the entry, string
* ID         : ID of the entry, integer

#### Output
```
{
	"type": "POST",
	"id": 1,
	"timestamp": 1224455,
	"signature": "abdbsnbdsbndbs",
	"ipfs": "koukouroukou"
}
```

---

### InitLedger

#### Summary
Initializes the ledger with some values for testing

---

### QueryEntryHistory

#### Summary
Returns an array of history changes for a specific entry along with their TxId, TimeStamp and IsDelete flag.

#### Arguments
* Type       : Type of the entry, string
* ID         : ID of the entry, integer

#### Output
```
[{
	"TxId": "6952f2a0bdcb43a0cd5ac6ac5a3dbccb5d50b14b3dc330cec664463374738f97",
	"Value": {
		"type": "POST",
		"id": 1,
		"timestamp": 1224455,
		"signature": "abdbsnbdsbndbs",
		"ipfs": "koukouroukou"
	},
	"Timestamp": "2019-11-15 07:36:15.364754035 +0000 UTC",
	"IsDelete": "false"
}]
```

---

### CreateEntry

#### Summary
CreateEntry creates a new entry, the key is the Type + ID, All the data is serialized into a json structure for easier retrieval.

#### Arguments
* Type       : Type of the entry, string
* ID         : ID of the entry, integer
* Timestamp  : Timestamp of the entry, integer
* Signature  : Signature of the entry, string, hash
* Ipfs       : Ipfs address of storage, string, hash

#### Output

True or False

### DeleteEntry

#### Summary
Deletes the entry with key Type + ID

#### Arguments
* Type       : Type of the entry, string
* ID         : ID of the entry, integer

#### Output

True-False
---

## Produce documentation

To produce documentation for the file you will need to have installed the ```go``` language and supporting environment, as well as the ```godoc``` package.

### Setup
```
sudo apt install golang golang-doc
```

### Run
```
godoc -templates=/usr/share/golang-golang-x-tools/godoc/static/  -html `realpath ./chaincode/` > index.html
```
