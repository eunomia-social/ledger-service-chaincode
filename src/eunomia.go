// Package main implements the chaincode for the EUNOMIA blockchain project
// Converts the blockchain into a key-value store with history
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

// SmartContract defines the Smart Contract structure
type SmartContract struct {
}

// Entry is the entry structure,
// Structure tags are used by encoding/json library
type Entry struct {
	ObjectType string `json:"docType"`
	Type       string `json:"type"`
	ID         int    `json:"id"`
	Timestamp  int    `json:"timestamp"`
	Signature  string `json:"signature"`
	Ipfs       string `json:"ipfs"`
}

type EntryID struct {
	ID   string `json:"id"`
	Type string `json:"type"`
}

// Init method is called when the Smart Contract "eunomia" is instantiated by the blockchain network
// Best practice is to have any Ledger initialization in separate function -- see initLedger()
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

// Invoke is called as a result of an application request to run the Smart Contract "eunomia"
// The calling application program has also specified the particular
// smart contract function to be called, with generic arguments
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()
	// Route to the appropriate handler function to interact with the ledger appropriately
	if function == "queryEntry" {
		return s.QueryEntry(APIstub, args)
	} else if function == "queryEntries" {
		return s.QueryEntries(APIstub, args)
	} else if function == "queryEntryHistory" {
		return s.QueryEntryHistory(APIstub, args)
	} else if function == "initLedger" {
		return s.InitLedger(APIstub)
	} else if function == "createEntry" {
		return s.CreateEntry(APIstub, args)
	} else if function == "deleteEntry" {
		return s.DeleteEntry(APIstub, args)
	} else if function == "queryTypeEntriesHistory" {
		return s.QueryTypeEntriesHistory(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

// QueryEntry returns the Entry with 2 arguments
// The first one is the Type of the entry
// The second one is the ID of the entry
func (s *SmartContract) QueryEntry(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	entryKey := args[0] + args[1]
	entryAsBytes, err := APIstub.GetState(entryKey)
	fmt.Printf("- queryEntry key: %s\n", entryKey)

	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(entryAsBytes)
}

// QueryEntries returns the Entries of the input {type,id} json array
// The first one is the Type of the entry
// The second one is the ID of the entry
func (s *SmartContract) QueryEntries(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1 json array")
	}

	// fmt.Printf("- queryEntries: %T %s\n", args[0], args[0])
	var entriesID []EntryID
	err := json.Unmarshal([]byte(args[0]), &entriesID)

	// entriesID, err := json.Marshal(args[0])
	if err != nil {
		return shim.Error(err.Error())
	}

	// fmt.Printf("- queryEntries IDs: %s\n", entriesID)
	var resEntries []Entry

	i := 0
	for i < len(entriesID) {
		eid := entriesID[i] //EntryID{}
		// err := json.Unmarshal(entriesID[i], &eid)
		// if err != nil {
		// 	return shim.Error(err.Error())
		// }

		entryKey := eid.Type + eid.ID //args[0] + args[1]
		entryAsBytes, err := APIstub.GetState(entryKey)
		// fmt.Printf("- queryEntry key: %s\n", entryKey)
		// fmt.Printf("- queryEntry keybytes: %T %s\n", entryAsBytes, entryAsBytes)
		if err != nil {
			return shim.Error(err.Error())
		}
		if len(entryAsBytes) == 0 {
			i++
			continue
		}
		var mentry Entry
		err = json.Unmarshal(entryAsBytes, &mentry)
		if err != nil {
			return shim.Error(err.Error())
		}
		resEntries = append(resEntries, mentry)
		i++
	}

	fmt.Printf("- queryEntries res: %s\n", resEntries)

	resBytes, err := json.Marshal(resEntries)
	if err != nil {
		return shim.Error(err.Error())
	}
	// fmt.Printf("- queryEntries resbytes: %s\n", resBytes)
	return shim.Success(resBytes)
}

// QueryEntryHistory returns the History of Entry with 2 arguments
// The first one is the Type of the entry
// The second one is the ID of the entry
func (s *SmartContract) QueryEntryHistory(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	entryKey := args[0] + args[1]

	// GetHistoryForKey requires peer configuration
	// core.ledger.history.enableHistoryDatabase to be true.
	resultsIterator, err := APIstub.GetHistoryForKey(entryKey)

	fmt.Printf("- queryEntryHistory key: %s\n", entryKey)

	if err != nil {
		return shim.Error(err.Error())
	}

	var buffer bytes.Buffer

	defer resultsIterator.Close()

	buffer.WriteString("[")

	arrayMemberAlreadyWritten := false

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()

		if err != nil {
			return shim.Error(err.Error())
		}

		// Add a comma before array members, suppress it for the first array member
		if arrayMemberAlreadyWritten == true {
			buffer.WriteString(",")
		}

		buffer.WriteString("{\"TxId\":")
		buffer.WriteString("\"")
		buffer.WriteString(queryResponse.TxId)
		buffer.WriteString("\"")
		buffer.WriteString(", \"Value\":")

		// if it was a delete operation on given key, then we need to set the
		// corresponding value null. Else, we will write the queryResponse.Value
		// as-is (as the Value itself a JSON object)
		if queryResponse.IsDelete {
			buffer.WriteString("null")
		} else {
			buffer.WriteString(string(queryResponse.Value))
		}

		buffer.WriteString(", \"Timestamp\":")
		buffer.WriteString("\"")
		buffer.WriteString(time.Unix(queryResponse.Timestamp.Seconds, int64(queryResponse.Timestamp.Nanos)).String())
		buffer.WriteString("\"")
		buffer.WriteString(", \"IsDelete\":")
		buffer.WriteString("\"")
		buffer.WriteString(strconv.FormatBool(queryResponse.IsDelete))
		buffer.WriteString("\"")
		buffer.WriteString("}")

		arrayMemberAlreadyWritten = true
	}

	buffer.WriteString("]")

	return shim.Success(buffer.Bytes())
}

// InitLedger Initializes the ledger with a few Entries
// This is used for testing purposes
func (s *SmartContract) InitLedger(APIstub shim.ChaincodeStubInterface) sc.Response {
	entries := []Entry{
		Entry{ObjectType: "EunomiaEntry", Type: "POST", ID: 1, Timestamp: 1224455, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"},
		Entry{ObjectType: "EunomiaEntry", Type: "POST", ID: 2, Timestamp: 1224555, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"},
		Entry{ObjectType: "EunomiaEntry", Type: "POST", ID: 3, Timestamp: 1224655, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"},
		Entry{ObjectType: "EunomiaEntry", Type: "CASCADE", ID: 1, Timestamp: 1224455, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"},
		Entry{ObjectType: "EunomiaEntry", Type: "CASCADE", ID: 2, Timestamp: 1224555, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"},
		Entry{ObjectType: "EunomiaEntry", Type: "CASCADE", ID: 3, Timestamp: 1224655, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"},
		Entry{ObjectType: "EunomiaEntry", Type: "USER", ID: 1, Timestamp: 1224455, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"},
		Entry{ObjectType: "EunomiaEntry", Type: "USER", ID: 2, Timestamp: 1224555, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"},
		Entry{ObjectType: "EunomiaEntry", Type: "USER", ID: 3, Timestamp: 1224655, Signature: "abdbsnbdsbndbs", Ipfs: "koukouroukou"}}

	i := 0
	for i < len(entries) {
		fmt.Println("i is ", i)
		entryAsBytes, err := json.Marshal(entries[i])

		if err != nil {
			return shim.Error(err.Error())
		}

		// concat TYPE and Id so they are unique
		APIstub.PutState(entries[i].Type+strconv.Itoa(entries[i].ID), entryAsBytes)
		fmt.Println("Added", entries[i])
		i = i + 1
	}

	return shim.Success(nil)
}

// CreateEntry creates a new entry, the key is the Type + ID,
// All the data is serialized into a json structure for easier retrieval
func (s *SmartContract) CreateEntry(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	eType := args[0]

	// parsing Id
	eID, err := strconv.Atoi(args[1])

	if err != nil {
		return shim.Error(err.Error())
	}

	eTimestamp, err := strconv.Atoi(args[2])

	if err != nil {
		return shim.Error(err.Error())
	}

	eSignature := args[3]
	eIpfsReference := args[4]

	var entry = Entry{
		ObjectType: "EunomiaEntry",
		Type:       eType,
		ID:         eID,
		Timestamp:  eTimestamp,
		Signature:  eSignature,
		Ipfs:       eIpfsReference}

	entryAsBytes, err := json.Marshal(entry)

	if err != nil {
		return shim.Error(err.Error())
	}

	entryKey := entry.Type + strconv.Itoa(entry.ID)
	APIstub.PutState(entryKey, entryAsBytes)

	fmt.Println("Added", entryKey)

	return shim.Success(nil)
}

//DeleteEntry marks an entry as deleted. The key is the Type + ID
func (s *SmartContract) DeleteEntry(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	eType := args[0]

	fmt.Println("DeleteEntry called", eType)
	// parsing Id
	eID, err := strconv.Atoi(args[1])
	if err != nil {
		return shim.Error(err.Error())
	}

	entryKey := eType + strconv.Itoa(eID)
	err = APIstub.DelState(entryKey)
	if err != nil {
		return shim.Error("Failed to delete state")
	}

	return shim.Success(nil)
}

// QueryTypeEntriesHistory
// it queries the history of all the entries of a given type
func (s *SmartContract) QueryTypeEntriesHistory(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	eType := args[0]
	fmt.Printf("- queryTypeEntriesHistory key: %s\n", eType)

	queryString := fmt.Sprintf("{\"selector\":{\"type\":\"%s\"}}", eType)

	queryResults, err := APIstub.GetQueryResult(queryString)

	if err != nil {
		return shim.Error(err.Error())
	}
	// return shim.Success(queryResults)

	var resbuffer bytes.Buffer

	defer queryResults.Close()

	resbuffer.WriteString("[")

	arrayMemberAlreadyWritten := false

	for queryResults.HasNext() {
		queryResponse, err := queryResults.Next()

		if err != nil {
			return shim.Error(err.Error())
		}

		// fmt.Printf("- queryEntryHistory key: %s\n", queryResponse._id)

		// Add a comma before array members, suppress it for the first array member
		if arrayMemberAlreadyWritten == true {
			resbuffer.WriteString(",")
		}

		// Retrieve the history of each entry
		// GetHistoryForKey requires peer configuration
		// core.ledger.history.enableHistoryDatabase to be true.
		histQueryResult, err := APIstub.GetHistoryForKey(queryResponse.Key)

		fmt.Printf("- queryEntryHistory key: %s\n", queryResponse.Key)

		if err != nil {
			return shim.Error(err.Error())
		}

		var buffer bytes.Buffer

		defer histQueryResult.Close()

		buffer.WriteString("[")
		arrayMemberAlreadyWritten_1 := false

		for histQueryResult.HasNext() {
			histQueryResponse, err := histQueryResult.Next()

			if err != nil {
				return shim.Error(err.Error())
			}

			// Add a comma before array members, suppress it for the first array member
			if arrayMemberAlreadyWritten_1 == true {
				buffer.WriteString(",")
			}

			buffer.WriteString("{\"TxId\":")
			buffer.WriteString("\"")
			buffer.WriteString(histQueryResponse.TxId)
			buffer.WriteString("\"")
			buffer.WriteString(", \"Value\":")

			// if it was a delete operation on given key, then we need to set the
			// corresponding value null. Else, we will write the queryResponse.Value
			// as-is (as the Value itself a JSON object)
			if histQueryResponse.IsDelete {
				buffer.WriteString("null")
			} else {
				buffer.WriteString(string(histQueryResponse.Value))
			}

			buffer.WriteString(", \"Timestamp\":")
			buffer.WriteString("\"")
			buffer.WriteString(time.Unix(histQueryResponse.Timestamp.Seconds, int64(histQueryResponse.Timestamp.Nanos)).String())
			buffer.WriteString("\"")
			buffer.WriteString(", \"IsDelete\":")
			buffer.WriteString("\"")
			buffer.WriteString(strconv.FormatBool(histQueryResponse.IsDelete))
			buffer.WriteString("\"")
			buffer.WriteString("}")

			arrayMemberAlreadyWritten_1 = true

		}
		buffer.WriteString("]")

		// unmarshall it
		mentry := Entry{}
		err = json.Unmarshal(queryResponse.Value, &mentry)
		if err != nil {
			return shim.Error(err.Error())
		}

		// and write the result object
		resbuffer.WriteString("{\"id\":")
		// resbuffer.WriteString("\"")
		// resbuffer.WriteByte(mentry.ID)
		// resbuffer.WriteString(strconv.FormatInt(mentry.ID,64))
		resbuffer.WriteString(strconv.Itoa(mentry.ID))
		// resbuffer.WriteString("\"")
		resbuffer.WriteString(",\"type\":")
		resbuffer.WriteString("\"")

		resbuffer.WriteString(mentry.Type) //queryResponse.Value.type )

		resbuffer.WriteString("\"")
		resbuffer.WriteString(",\"history\":")
		// resbuffer.WriteString("\"")
		resbuffer.WriteString(buffer.String())
		resbuffer.WriteString("}") //"\"}")
		//

		arrayMemberAlreadyWritten = true
	}

	resbuffer.WriteString("]")

	return shim.Success(resbuffer.Bytes())
}

// main  function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))

	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
