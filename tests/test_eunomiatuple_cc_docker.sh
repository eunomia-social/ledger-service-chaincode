
#!/bin/bash

# Load common variables
# . ./config/network_env.sh

TEST_SUCCESS="[SUCCESS]"
TEST_FAILED="[FAIL]"


function setGlobals(){
    # Load common variables
    source ./config/network_env.sh

    #CC_RUNTIME_LANGUAGE=$LANGUAGE
    # path for chaincode in the docker containers
    #CC_SRC_PATH=github.com/chaincode/
    #CC_NAME=$CHAINCODE
    #CC_VERSION=$CHAINCODE_VERSION

    #CONFIG_ROOT=/opt/gopath/src/github.com/hyperledger/fabric/peer
    #ORG1_MSPCONFIGPATH=${CONFIG_ROOT}/crypto/peerOrganizations/${ORG_DOMAIN}/users/Admin@${ORG_DOMAIN}/msp
    #ORG1_TLS_ROOTCERT_FILE=${CONFIG_ROOT}/crypto/peerOrganizations/${ORG_DOMAIN}/peers/peer0.${ORG_DOMAIN}/tls/ca.crt
    #ORDERER_TLS_ROOTCERT_FILE=${CONFIG_ROOT}/crypto/ordererOrganizations/${NETWORK_DOMAIN}/orderers/orderer.${NETWORK_DOMAIN}/msp/tlscacerts/tlsca.${NETWORK_DOMAIN}-cert.pem

}


function installChaincode(){
    setGlobals

    echo "Installing smart contract on peer0.${ORG_DOMAIN}"
    docker exec \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_ADDRESS=peer0.${ORG_DOMAIN}:7051 \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    -e CORE_PEER_TLS_ROOTCERT_FILE=${ORG1_TLS_ROOTCERT_FILE} \
    cli \
    peer chaincode install \
        -n ${CC_NAME} \
        -v ${CC_VERSION} \
        -p "$CC_SRC_PATH" \
        -l "$CC_RUNTIME_LANGUAGE"

}

function instatiateChaincode(){
    
    setGlobals
    
    echo "Instantiating smart contract on ${CHANNEL_NAME}"
    
    docker exec \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode instantiate \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -l "$CC_RUNTIME_LANGUAGE" \
        -v ${CC_VERSION} \
        -c '{"Args":[]}' \
        -P '${ORG_NAME}MSP.member' \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE}
    
}

function initiateChaincode(){
    setGlobals

    echo "Submitting initLedger transaction to smart contract on ${CHANNEL_NAME}"
    echo "The transaction is sent to the two peers with the chaincode installed (peer0.${ORG_DOMAIN} ) so that chaincode is built before receiving the following requests"
    docker exec \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"initLedger","Args":[]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \

}


function test_queryEntryHistory(){
    setGlobals
    ## Load implemented function in chaincode repo
    #. ${CHAINCODE_TESTS_PATH}/test_eunomia_cc_docker.sh 
    #set -x
    ## call test functions one-by-one here
    echo "Testing queryEntryHistory transaction to smart contract on ${CHANNEL_NAME}"
    res=$(docker exec -it\
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"queryEntryHistory","Args":["TEST","4"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE})

    mstatus=$(echo $res | sed -n -e's/^.*status://p' | sed -e's/ .*//')
    if [[ "$mstatus" != 200 ]]; then
	echo "    $TEST_FAILED :  $res"
	return 1;
    fi;
    mpayload=$(echo $res | sed -n -e 's/^.*payload://' -e 's/^\"//' -e 's/\]\"/\]/' -e 's/\\\"/\"/gp')
    echo "    $TEST_SUCCESS"
#    echo "    $TEST_SUCCESS : 
#		$(echo $mpayload | jq .)"
    #set +x
}


function test_createEntry(){
    setGlobals
    ## Load implemented function in chaincode repo
    #. ${CHAINCODE_TESTS_PATH}/test_eunomia_cc_docker.sh 

    ## call test functions one-by-one here
    echo "Testing createEntry() transaction to smart contract on ${CHANNEL_NAME}"
    #echo "The transaction is sent to the one peer with the chaincode installed (peer0.${ORG_DOMAIN}) so that chaincode is built before receiving the following requests"
    res=$(docker exec -it \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"createEntry","Args":["TEST","4","111111","1NaAxX16TkgQsY6vCVvsyD1tvxWLj6oU5d","GCCV443Y6JZHZNWI36KGB46RL6UC7TMQS3JTXH2CLGMYCMZQFG35NECH"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE})

    mstatus=$(echo $res | sed -n -e's/^.*status://p' | sed -e's/ .*//')

    if [[ $mstatus != 200 ]]; then
	echo "    $TEST_FAILED : Create Entry failed!!! $res"
	return 1;
    fi;

    res=$(docker exec -it \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"queryEntry","Args":["TEST", "4"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE})

    mstatus=$(echo $res | sed -n -e's/^.*status://p' | sed -e's/ .*//')

    if [[ $mstatus != 200 ]]; then
	echo "    $TEST_FAILED : Query created Entry failed!!! $res"
	return 1;
    fi;
   
    mpayload=$(echo $res | sed -n -e 's/^.*payload://' -e 's/^\"//' -e 's/\}\"/\}/' -e 's/\\\"/\"/gp')
    mtype="$(echo $mpayload | jq .type ) == \"TEST\""
    mid="$(echo $mpayload | jq .id ) == \"4\""
    #echo $mtype , $mid
    if [[ $mtype && $mid ]] ; then
	echo "    $TEST_SUCCESS"
	return 0
    fi;
    echo "    $TEST_FAILED : Returned payload was not the expected one. $res"
    return 1

}


function test_queryEntry(){
    setGlobals
    ## Load implemented function in chaincode repo
    #. ${CHAINCODE_TESTS_PATH}/test_eunomia_cc_docker.sh 

    ## call test functions one-by-one here
    echo "Testing queryEntry transaction to smart contract on ${CHANNEL_NAME}"
    echo "    It queries entry (TEST,1) as it was created during Ledger Initialization."
    #echo "The transaction is sent to the two peers with the chaincode installed (peer0.${ORG_DOMAIN}) so that chaincode is built before receiving the following requests"
    res=$(docker exec -it \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"queryEntry","Args":["TEST", "1"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE})

    mstatus=$(echo $res | sed -n -e's/^.*status://p' | sed -e's/ .*//')
    if [[ $mstatus != 200 ]]; then
	echo "    $TEST_FAILED : It returned $res"
	return 1;
    fi;
    mpayload=$(echo $res | sed -n -e 's/^.*payload://' -e 's/^\"//' -e 's/\}\"/\}/' -e 's/\\\"/\"/gp')
    mtype="$(echo $mpayload | jq .type ) == \"TEST\""
    mid="$(echo $mpayload | jq .id ) == \"1\""
    #echo $mtype , $mid
    if [[ $mtype && $mid ]] ; then
	echo "    $TEST_SUCCESS"
	return 0
    else
	echo "    $TEST_FAILED : $res"
	return 1
    fi;
}

function test_deleteEntry(){
    # we need record 4 for this
    echo "Testing deleteEntry transaction to smart contract on ${CHANNEL_NAME}"
    echo "    First call createEntry test to create entry (TEST,4)"
    a=$(test_createEntry)
    #echo "tsa : $a"
    if [[ -n "$(echo $a | grep $TEST_FAILED)" ]]; then
	echo "    $TEST_FAILED"
	return 1
    fi;

    setGlobals
    ## Load implemented function in chaincode repo
    #. ${CHAINCODE_TESTS_PATH}/test_eunomia_cc_docker.sh 

    ## call test functions one-by-one here
#    echo "Testing Deleting Entry transaction to smart contract on ${CHANNEL_NAME}"
    #echo "The transaction is sent to the two peers with the chaincode installed (peer0.${ORG_DOMAIN}) so that chaincode is built before receiving the following requests"
    res=$( docker exec -it \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"deleteEntry","Args":["TEST", "4"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE})

    mstatus=$(echo $res | sed -n -e's/^.*status://p' | sed -e's/ .*//')
    if [[ $mstatus != 200 ]]; then
        echo "    $TEST_FAILED : $res"
	return 1;
    fi;

    res=$(docker exec -it \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"queryEntry","Args":["TEST", "4"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE})

    mstatus=$(echo $res | sed -n -e's/^.*status://p' | sed -e's/ .*//')
    if [[ $mstatus != 200 ]]; then
        echo "    $TEST_FAILED : $res"
	return 1;
    fi;
    mpayload=$(echo $res | sed -n -e 's/^.*payload://' -e 's/^\"//' -e 's/\}\"/\}/' -e 's/\\\"/\"/gp')
    mtype="$(echo $mpayload | jq .type ) == \"TEST\""
    mid="$(echo $mpayload | jq .id ) == \"4\""
    #echo $mtype , $mid
    if [[ $mtype && $mid ]] ; then
        echo "    $TEST_SUCCESS"
	return 0
    else
	echo "    $TEST_FAILED : $res"
	return 1
    fi;

}

function test_queryEntryHistory(){
    setGlobals
    ## Load implemented function in chaincode repo
    #. ${CHAINCODE_TESTS_PATH}/test_eunomia_cc_docker.sh 

    ## call test functions one-by-one here
    echo "Submitting queryEntryHistory transaction to smart contract on ${CHANNEL_NAME}"
    echo "The transaction is sent to the two peers with the chaincode installed (peer0.${ORG_DOMAIN}) so that chaincode is built before receiving the following requests"
    docker exec \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"queryEntryHistory","Args":["POST", "123"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE}
}


function test_deleteEntry(){
    setGlobals
    ## Load implemented function in chaincode repo
    #. ${CHAINCODE_TESTS_PATH}/test_eunomia_cc_docker.sh 

    ## call test functions one-by-one here
    echo "Submitting deleteEntry transaction to smart contract on ${CHANNEL_NAME}"
    echo "The transaction is sent to the two peers with the chaincode installed (peer0.${ORG_DOMAIN}) so that chaincode is built before receiving the following requests"
    docker exec \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"deleteEntry","Args":["POST", "123"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE}
}



function test_queryTypeEntriesHistory(){
    setGlobals
    ## Load implemented function in chaincode repo
    #. ${CHAINCODE_TESTS_PATH}/test_eunomia_cc_docker.sh 

    ## call test functions one-by-one here
    echo "Submitting queryTypeEntriesHistory transaction to smart contract on ${CHANNEL_NAME}"
    echo "The transaction is sent to the two peers with the chaincode installed (peer0.${ORG_DOMAIN}) so that chaincode is built before receiving the following requests"
    docker exec \
    -e CORE_PEER_LOCALMSPID=${ORG_NAME}MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.${NETWORK_DOMAIN}:7050 \
        -C ${CHANNEL_NAME} \
        -n ${CC_NAME} \
        -c '{"function":"queryTypeEntriesHistory","Args":["POST"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.${ORG_DOMAIN}:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE}
}


#####################################################################################################################################3333

function testChaincode(){

    setGlobals

    #. ./helper_scripts/util_functions.sh

    CLI_DELAY=2
    ## Load implemented function in chaincode repo
    #. ${CHAINCODE_TESTS_PATH}/test_eunomia_cc_docker.sh 

    ## call test functions one-by-one here
    #set -x
    set +e

    #echo $(create_test_entry "TEST" "5")
    #echo $(do_queryEntry "TEST" "5")

    test_queryEntry

    sleep ${CLI_DELAY} 

    test_createEntry

    sleep ${CLI_DELAY} 

    #test_queryEntry
    #sleep ${CLI_DELAY} 
    test_deleteEntry
    #sleep ${CLI_DELAY} 
    #test_queryEntry
    sleep ${CLI_DELAY} 
    test_queryEntryHistory

    #set +x
    set -e
}


#############
# Load variables
source ./config/network_env.sh

# Load helper functions

#. ./helper_scripts/util_functions.sh

# Do testing

testChaincode

#exit 0
